#!/bin/bash

# export PATH=$PATH:/opt/scripts

# Converte "Fri, 20 Mar 2020 16:14:48 -0300" TO "20/03/2020 16:14:48"
weekDay(){

	AUX=()
	count=0

	IFS=' '
	read -ra AUXSTR <<< "$TIME"
	for j in "${AUXSTR[@]}"; do
	    AUX+=("$j")
	done

	vet=('Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec')
	num=('01' '02' '03' '04' '05' '06' '07' '08' '09' '10' '11' '12')

	for ((i=0; i < "${#vet[@]}"; i++)); do

	    if [[ ${AUX[2]} == ${vet[$i]} ]]; then
	    	AUX[2]=${num[$count]}
	    fi

	    count=$count+1

	done

	STRING="${AUX[1]}/${AUX[2]}/${AUX[3]} ${AUX[4]}"

	echo $STRING

}

# Formata a data para a data aceita pela a API
convertDateToAPI(){

	WEEK="$(date --date="$1" '+%a')%2C%20"
	DAY="$(date --date="$1" '+%d')%20"
	MONTH="$(date --date="$1" '+%b')%20"
	YEAR="$(date --date="$1" '+%Y')%20"
	HOUR="$(date --date="$1" '+%H')%3A"
	MINUTE="$(date --date="$1" '+%M')%3A"
	SECOND="$(date --date="$1" '+%S')%20"
	GMT="$(date --date="$1" '+%z')"
	# GMT="$(date --date="$1" '+%z' | tr -d "-")"

	echo $WEEK$DAY$MONTH$YEAR$HOUR$MINUTE$SECOND$GMT

}

extractUsernameSuccessfulLogin() {
	# $1 = STRING

	STR=${1#*[accountName=}

	IFS=']'
	read -ra AUXSTR <<< "$STR"
	for j in "${AUXSTR[@]}"; do
		AUX+=("$j")
	done

	echo $(echo ${AUX} | tr -d ']')

}

extractUsernameDenyingAccess() {
	IFS=' '
	read -ra AUXSTR <<< "$1"
	for j in "${AUXSTR[@]}"; do
	    AUX+=("$j")
	done

	echo ${AUX[4]}
}

extractUsernameErrorDuringTransfer() {
	
	echo "$1"

	if [[ $1 == "Error during transfer operation: SMB"* ]]; then
		
		IFS=' '
		read -ra AUXSTR <<< "$2"
		for j in "${AUXSTR[@]}"; do
		    AUX+=("$j")
		done

		STR=$(echo ${AUX[4]//"&quot;"/"" })

		echo $(echo ${STR} | tr -d ',')

	elif [[ $1 == "Error during transfer operation: File"* ]]; then

		IFS=','
		read -ra AUXSTR <<< "$2"
		for j in "${AUXSTR[@]}"; do
		    AUX+=("$j")
		done

		STR=$(echo ${AUX[4]//"user="/"" })

		echo $STR

	fi
}

extractSessionIdentifier(){
		delimiter=sessionIdentifier=
		str=$1$delimiter
		array=();
		while [[ $str ]]; do
		    array+=( "${str%%"$delimiter"*}" );
		    str=${str#*"$delimiter"};
		done;
		# declare -p array

		echo $(echo ${array[1]} | tr -d '"')
}


# Extrai a data para o log
getDate(){
	DATETIME=$(date '+%d/%m/%Y %H:%M:%S')

	echo "$DATETIME | "

}

# Servidores - Backend e Edge, respectivamente
# SERVER=("https://localhost:8444/api/v1.4" "IP - EDGE")
# USER=("userBACKEND" "senhaBACKEND")
# PASS=("userEdge" "senhaEdge")

SERVER=("https://18.210.136.229:444/api/v1.4")
USER=("bruno")
PASS=("edilabs2019")

# Datas a serem buscadas
FROM=$(convertDateToAPI '1 hour ago')
END=$(convertDateToAPI 'now')

# Arquivo de log
LOG_FILE="./logs.log"

OUTPUT="output/teste/"

if [[ ! -d $OUTPUT ]]; then
	mkdir $OUTPUT
fi

# Limite de ocorrências a serem buscadas no servidor
# LIMIT=1000000000
LIMIT=10

# echo $FROM
# echo $END

# Mensagens a serem buscadas no servidor
MESSAGES=("Successful%20login%20on" "Denying%20access%20to%20user" "Error%20during%20transfer%20operation" "Error%20during%20transfer%20operation%3A%20File")
MESSAGESLOG=("Successful login on" "Denying access to user" "Error during transfer operation")


# DISPLAY
echo $(getDate) "Logs sendo extraídos de '$(date --date="1 hour ago")' até '$(date --date="now")'"

# LOG
echo $(getDate) "Logs sendo extraídos de '$(date --date="1 hour ago")' até '$(date --date="now")'" >> $LOG_FILE
echo 

lala=0

for ((i=0; i < "${#SERVER[@]}"; i++)); do


	for ((j=0; j < "${#MESSAGES[@]}"; j++)); do

		# PARAMETROS COM DATA
		# PARAMETERS="/logs?limit=10&component=TM&component=SSHD&component=AUDIT&component=FTPD&component=HTTPD&level=INFO&level=WARN&level=ERROR&fromDate=$FROM&endDate=$END&message=${MESSAGES[$j]}"
		PARAMETERS="/logs?limit=$LIMIT&message=${MESSAGES[$j]}"
		# PARAMETERS="/logs?limit=$LIMIT&fromDate=$FROM&endDate=$END&message=${MESSAGES[$j]}"

		# DISPLAY
		echo $(getDate) "Acessando servidor: ${SERVER[$i]}"
		echo $(getDate) "Procurando por mensagem '${MESSAGESLOG[$j]}'"

		# LOG
		echo $(getDate) "Acessando servidor: ${SERVER[$i]}" >> $LOG_FILE
		echo $(getDate) "Procurando por mensagem '${MESSAGESLOG[$j]}'" >> $LOG_FILE

		curl -s -k -u ${USER[$i]}:${PASS[$i]} -X GET --header 'Accept: application/json' --output ./serverlog.json ${SERVER[$i]}$PARAMETERS

		NUM=$(jq '.logEntries | length' serverlog.json)
		
		echo $NUM

		echo $(getDate) "Logs encontrados: $NUM"
		echo $(getDate) "Logs encontrados: $NUM" >> $LOG_FILE

		echo

		for ((k=0; k < $NUM; k++)); do

			LEVEL=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.level)
			TIME=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.time)
			COMPONENT=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.component)
			THREAD=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.thread)
			FILENAME=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.filename)
			CLASSNAME=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.classname)
			METHOD=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.method)
			LINE=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.line)
			MESSAGE=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.message)

			SESSION=$(cat ./serverlog.json | jq .logEntries[$k].logEntry.metadata.links.session)

			IDSESSION=$(extractSessionIdentifier "$SESSION")

			echo $IDSESSION

			TIME=$(weekDay)

			# COMPONENT_AUX=$(echo $COMPONENT | tr -d "D;")



			if [[ $j == 0 ]]; then
				USERNAME=$(extractUsernameSuccessfulLogin "$MESSAGE")
				
			elif [[ $j == 1 ]]; then
				USERNAME=$(extractUsernameDenyingAccess "$MESSAGE")
			
			elif [[ $j == 2 ]]; then
				
				if [[ $MESSAGE == "Error during transfer operation: SMB"* ]]; then

				curl -s -k -u ${USER[$i]}:${PASS[$i]} -X GET --header 'Accept: application/json' --output ./sessionId.json "${SERVER[$i]}/logs?sessionIdentifier=$IDSESSION"
 
				NUM_SESSION=$(jq '.logEntries | length' sessionId.json)

				echo $NUM_SESSION

				for ((l=0; l < "$NUM_SESSION"; l++)); do
				
					METHOD_SESSION=$(cat ./sessionId.json | jq .logEntries[$l].logEntry.method)

					# echo $METHOD_SESSION

					METHOD_SESSION=$(echo $METHOD_SESSION | tr -d '"')

					MSG_AUX=""

					if [[ $MESSAGE == "Error during transfer operation: SMB"* ]]; then
						MSG_AUX="logSecurityInformation"
					elif [[ $MESSAGE == "Error during transfer operation: File"* ]]; then
						MSG_AUX="logTransfer"
					fi	
					
					if [[ $METHOD_SESSION == $MSG_AUX ]]; then

						MESSAGE_SESSION=$(cat ./sessionId.json | jq .logEntries[$l].logEntry.message)
						USERNAME=$(extractUsernameErrorDuringTransfer "$MESSAGE" "$MESSAGE_SESSION")

						break
					fi
				done
			fi


			echo $USERNAME

			# Verificação para separar os logs do BackEnd e Edge
			if [[ $i -eq 0 ]]; then
				echo "$TIME | $LEVEL | $COMPONENT | $THREAD | $FILENAME | $CLASSNAME | $METHOD | $LINE | $USERNAME | $MESSAGE" >> "$OUTPUT/BACKEND_$(date '+%Y%m%d').txt"
			else
				echo "$TIME | $LEVEL | $COMPONENT | $THREAD | $FILENAME | $CLASSNAME | $METHOD | $LINE | $USERNAME | $MESSAGE" >> "$OUTPUT/EDGE_$(date '+%Y%m%d').txt"
			fi
			
			USERNAME=""

		done

		if [[ -f './serverlog.json' ]]; then
			rm './serverlog.json'
		fi

	done

done

echo >> $LOG_FILE

echo
echo "Terminado!"

exit

